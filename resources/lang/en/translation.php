<?php

return [

    /**
     * Singlular and plural name of the module
     */
    'name'        => 'Translation',
    'names'       => 'Translations',
    'user_name'   => 'My <span>Translation</span>',
    'user_names'  => 'My <span>Translations</span>',
    'create'      => 'Create My Translation',
    'edit'        => 'Update My Translation',

    /**
     * Options for select/radio/check.
     */
    'options'     => [

    ],

    /**
     * Placeholder for inputs
     */
    'placeholder' => [
        'parent_id'     => 'Please enter parent_id',
        'start'         => 'Please enter start',
        'end'           => 'Please enter end',
        'category'      => 'Please enter category',
        'translation'          => 'Please enter translation',
        'time_required' => 'Please enter time_required',
        'time_taken'    => 'Please enter time_taken',
        'priority'      => 'Please enter priority',
        'status'        => 'Please enter status',
        'created_by'    => 'Please enter created_by',
    ],

    /**
     * Labels for inputs.
     */
    'label'       => [
        'parent_id'     => 'Parent id',
        'start'         => 'Start',
        'end'           => 'End',
        'category'      => 'Category',
        'translation'          => 'Translation',
        'time_required' => 'Time required',
        'time_taken'    => 'Time taken',
        'priority'      => 'Priority',
        'status'        => 'Status',
        'created_by'    => 'Created by',
        'status'        => 'Status',
        'created_at'    => 'Created at',
        'updated_at'    => 'Updated at',
    ],

    /**
     * Tab labels
     */
    'tab'         => [
        'name' => 'Name',
    ],

    /**
     * Texts  for the module
     */
    'text'        => [
        'preview' => 'Click on the below list for preview',
    ],
];
