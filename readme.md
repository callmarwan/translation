This is a Laravel 5 package that provides translation management facility for souktel framework.

## Installation

Begin by installing this package through Composer. Edit your project's `composer.json` file to require `souktel/translation`.

    "souktel/translation": "dev-master"

Next, update Composer from the Terminal:

    composer update

Once this operation completes execute below cammnds in command line to finalize installation.

```php
SoukTel\Translation\Providers\TranslationServiceProvider::class,

```

And also add it to alias

```php
'Translation'  => SoukTel\Translation\Facades\Translation::class,
```

Use the below commands for publishing

Migration and seeds

    php artisan vendor:publish --provider="SoukTel\Translation\Providers\TranslationServiceProvider" --tag="migrations"
    php artisan vendor:publish --provider="SoukTel\Translation\Providers\TranslationServiceProvider" --tag="seeds"

Configuration

    php artisan vendor:publish --provider="SoukTel\Translation\Providers\TranslationServiceProvider" --tag="config"

Language

    php artisan vendor:publish --provider="SoukTel\Translation\Providers\TranslationServiceProvider" --tag="lang"

Views public and admin

    php artisan vendor:publish --provider="SoukTel\Translation\Providers\TranslationServiceProvider" --tag="view-public"
    php artisan vendor:publish --provider="SoukTel\Translation\Providers\TranslationServiceProvider" --tag="view-admin"

Publish admin views only if it is necessary.

## Usage


