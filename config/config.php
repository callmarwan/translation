<?php

return [

    /**
     * Provider.
     */
    'provider' => 'souktel',

    /*
     * Package.
     */
    'package' => 'translation',

    /*
     * Modules.
     */
    'modules' => ['translation'],

    'translation' => [
        'model' => 'SoukTel\Translation\Models\Translation',
        'table' => 'translations',
        'presenter' => \SoukTel\Translation\Repositories\Presenter\TranslationItemPresenter::class,
        'hidden' => [],
        'visible' => [],
        'guarded' => ['*'],
        'slugs' => ['slug' => 'translation'],
        'dates' => [],
        'appends' => [],
        'fillable' => ['status', 'locale', 'group', 'key', 'value'],

        'upload_folder' => '/translation/translation',
        'uploads' => [
            'single' => [],
            'multiple' => [],
        ],
        'casts' => [
        ],
        'revision' => [],
        'perPage' => '20',
        'search' => [
            'value' => 'like',
            'status',
        ],
    ],

    'delete_enabled' => true,

    /**
     * Exclude specific groups from Laravel Translation Manager.
     * This is useful if, for example, you want to avoid editing the official Laravel language files.
     *
     * @type array
     *
     *    array(
     *        'pagination',
     *        'reminders',
     *        'validation',
     *    )
     */
    'exclude_groups' => array('pagination', 'validation',),
];
