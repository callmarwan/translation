<?php

namespace SoukTel\Translation\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Form;
use SoukTel\Translation\Http\Requests\TranslationAdminRequest;
use SoukTel\Translation\Interfaces\TranslationRepositoryInterface;
use SoukTel\Translation\Manager;
use SoukTel\Translation\Models\Translation;

/**
 * Admin web controller class.
 */
class TranslationAdminController extends BaseController
{
    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    public $guard = 'admin.web';

    /**
     * The home page route of admin.
     *
     * @var string
     */
    public $home = 'admin';

    protected $manager;

    /**
     * TranslationAdminController constructor.
     * @param TranslationRepositoryInterface $translation
     * @param Manager $manager
     */
    public function __construct(TranslationRepositoryInterface $translation, Manager $manager)
    {
        $this->manager = $manager;
        $this->repository = $translation;
        $this->middleware('web');
        $this->middleware('auth:admin.web');
        $this->setupTheme(config('theme.themes.admin.theme'), config('theme.themes.admin.layout'));
        parent::__construct();
    }

    /**
     * @param null $group
     * @return mixed
     */
    public function index()
    {
        $locales = $this->loadLocales();
        $groups = Translation::groupBy('group');

        $excludedGroups = $this->manager->getConfig('exclude_groups');
        if ($excludedGroups) {
            $groups->whereNotIn('group', $excludedGroups);
        }

        $groups = $groups->pluck('group', 'group');
        if ($groups instanceof Collection) {
            $groups = $groups->all();
        }
        $groups = ['' => 'Choose a group'] + $groups->toArray();

        $group = null;

        $deleteEnabled = $this->manager->getConfig('delete_enabled');
        $this->theme->prependTitle(trans('translation::translation.names') . ' :: ');
        return $this->theme->of('translation::admin.translation.index',
            compact('translations', 'locales', 'groups','group', 'deleteEnabled'))->render();
    }

    protected function loadLocales()
    {
        //Set the default locale as the first one.
        $locales = Translation::groupBy('locale')->get()->pluck('locale');

        if ($locales instanceof Collection) {
            $locales = $locales->all();
        }

        $locales = array_merge([config('app.locale')], $locales->toArray());
        return array_unique($locales);
    }

    public function getView($group = '')
    {
        $locales = $this->loadLocales();
        $groups = Translation::groupBy('group');

        $excludedGroups = $this->manager->getConfig('exclude_groups');
        if ($excludedGroups) {
            $groups->whereNotIn('group', $excludedGroups);
        }

        $groups = $groups->pluck('group', 'group');
        if ($groups instanceof Collection) {
            $groups = $groups->all();
        }
        $groups = ['' => 'Choose a group'] + $groups->toArray();
        $numChanged = Translation::where('group', $group)->where('status', Translation::STATUS_CHANGED)->count();


        $allTranslations = Translation::where('group', $group)->orderBy('key', 'asc')->get();
        $numTranslations = count($allTranslations);
        $translations = [];
        foreach ($allTranslations as $translation) {
            $translations[$translation->key][$translation->locale] = $translation;
        }
        $deleteEnabled = $this->manager->getConfig('delete_enabled');
        $editUrl = url('admin/translation/translation/edit/' . $group);
        $this->theme->prependTitle(trans('translation::translation.names') . ' :: ');
        $this->theme->asset()->add('bootstrap-editable', '//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css');
        $this->theme->asset()->container('extra')->add('bootstrap-editable', '//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js');
        return $this->theme->of('translation::admin.translation.edit',
            compact('translations', 'locales', 'groups', 'group', 'numTranslations', 'numChanged',
                'editUrl', 'deleteEnabled'))->render();

    }

    public function postEdit(TranslationAdminRequest $request, $group)
    {
        if (!in_array($group, config('package.translation.exclude_groups'))) {
            $groups = func_get_args();
            array_shift($groups); // remove the $request
            $group = implode('/', $groups);
            $name = $request->get('name');
            $value = $request->get('value');

            list($locale, $key) = explode('|', $name, 2);
            $translation = Translation::firstOrNew([
                'locale' => $locale,
                'group' => $group,
                'key' => $key,
            ]);
            $translation->value = (string)$value ?: null;

            $translation->status = Translation::STATUS_CHANGED;
            $translation->save();
            return array('status' => 'ok');
        }
    }

    public function postImport(TranslationAdminRequest $request)
    {
        $replace = $request->get('replace', false);
        $counter = $this->manager->importTranslations($replace);

        return ['status' => 'ok', 'counter' => $counter];
    }

    public function postAdd(TranslationAdminRequest $request, $group)
    {
        $keys = explode("\n", $request->get('keys'));

        foreach ($keys as $key) {
            $key = trim($key);
            if ($group && $key) {
                $this->manager->missingKey('*', $group, $key);
            }
        }
        return redirect()->back();
    }

    public function postDelete($group, $key)
    {
        if (!in_array($group, $this->manager->getConfig('exclude_groups')) && $this->manager->getConfig('delete_enabled')) {
            Translation::where('group', $group)->where('key', $key)->delete();
            return ['status' => 'ok'];
        }
    }

    public function postFind()
    {
        $numFound = $this->manager->findTranslations();

        return ['status' => 'ok', 'counter' => (int)$numFound];
    }

    public function postPublish($group)
    {
        $this->manager->exportTranslations($group);

        return ['status' => 'ok'];
    }

}
