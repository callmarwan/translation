<?php

// Admin web routes  for translation
Route::group(['prefix' => trans_setlocale() . '/admin/translation'], function () {
    Route::get('translation', 'SoukTel\Translation\Http\Controllers\TranslationAdminController@index');
    Route::get('translation/view/{group}', 'SoukTel\Translation\Http\Controllers\TranslationAdminController@getView');
    Route::post('translation/edit/{group}', 'SoukTel\Translation\Http\Controllers\TranslationAdminController@postEdit');
    Route::post('translation/import', 'SoukTel\Translation\Http\Controllers\TranslationAdminController@postImport');
    Route::post('translation/postFind', 'SoukTel\Translation\Http\Controllers\TranslationAdminController@postFind');
    Route::post('translation/postPublish/{group}', 'SoukTel\Translation\Http\Controllers\TranslationAdminController@postPublish');
    Route::post('translation/postAdd/{group}', 'SoukTel\Translation\Http\Controllers\TranslationAdminController@postAdd');
    Route::post('translation/postDelete/{group}/{key}', 'SoukTel\Translation\Http\Controllers\TranslationAdminController@postDelete');
});

Route::group(['prefix' => trans_setlocale() . '/user/translation'], function () {
    Route::get('translation', 'SoukTel\Translation\Http\Controllers\TranslationUserController@index');
    Route::get('translation/view/{group}', 'SoukTel\Translation\Http\Controllers\TranslationUserController@getView');
    Route::post('translation/edit/{group}', 'SoukTel\Translation\Http\Controllers\TranslationUserController@postEdit');
//    Route::post('translation/import', 'SoukTel\Translation\Http\Controllers\TranslationUserController@postImport');
//    Route::post('translation/postFind', 'SoukTel\Translation\Http\Controllers\TranslationUserController@postFind');
//    Route::post('translation/postPublish/{group}', 'SoukTel\Translation\Http\Controllers\TranslationUserController@postPublish');
//    Route::post('translation/postAdd/{group}', 'SoukTel\Translation\Http\Controllers\TranslationUserController@postAdd');
//    Route::post('translation/postDelete/{group}/{key}', 'SoukTel\Translation\Http\Controllers\TranslationUserController@postDelete');
});