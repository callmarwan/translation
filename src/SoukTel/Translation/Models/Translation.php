<?php

namespace SoukTel\Translation\Models;

use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;
use Litepie\Revision\Traits\Revision;
use Litepie\Trans\Traits\Trans;
use Litepie\User\Traits\UserModel;

class Translation extends Model
{
    use PresentableTrait;
    const STATUS_SAVED = 0;
    const STATUS_CHANGED = 1;
    /**
     * Configuartion for the model.
     *
     * @var array
     */
    protected $config = 'package.translation.translation';

}
