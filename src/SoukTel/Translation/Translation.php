<?php

namespace SoukTel\Translation;

class Translation
{
    /**
     * $translation object.
     */
    protected $translation;

    /**
     * Constructor.
     */
    public function __construct(\SoukTel\Translation\Interfaces\TranslationRepositoryInterface $translation)
    {
        $this->translation = $translation;
    }

    /**
     * Display translations of the user.
     *
     * @return Response
     */
    public function display($view)
    {
        return view('translation::admin.translation.' . $view);
    }

    /**
     * Returns count of translations.
     *
     * @param array $filter
     *
     * @return int
     */
    public function count()
    {
        return $this->translation->getCount();
    }

    public function completed()
    {
        return $this->translation->completed();
    }

    public function todo()
    {
        return $this->translation->todo();
    }

    public function translations()
    {
        return $this->translation->translations();
    }

    /**
     * Returns gadgets.
     *
     * @param array $filter
     *
     * @return int
     */
    public function gadget($view = 'admin.translation.gadget', $count = 10)
    {
        $translations = $this->translation->gadget($count);

        return view('translation::' . $view, compact('translations'))->render();
    }
}
