<?php

namespace SoukTel\Translation\Repositories\Eloquent;

use SoukTel\Translation\Interfaces\TranslationRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class TranslationRepository extends BaseRepository implements TranslationRepositoryInterface
{
    /**
     * Booting the repository.
     *
     * @return null
     */
    public function boot()
    {
        $this->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'));
    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        $this->fieldSearchable = config('package.translation.translation.search');
        return config('package.translation.translation.model');
    }


    public function getCount()
    {
        return $this->model->count();
    }

    public function completed()
    {
        return $this->model->whereStatus('completed')->count();
    }

    public function translations()
    {
        return $this->model->with('user')->whereStatus('to_do')->orderBy('id', 'DESC')->get();
    }

    public function todo()
    {
        return $this->model->with('user')->orderBy('id', 'DESC')->take(6)->get();
    }

    public function gadget($count)
    {

        return $this->model->with('user')
                            ->where(function($query){
                                if (user('web')) {
                                    $query->whereUserId(user_id('web'));
                                }
                              })
                            ->orderBy('id', 'DESC')
                            ->take($count)
                            ->get();
    }
}
