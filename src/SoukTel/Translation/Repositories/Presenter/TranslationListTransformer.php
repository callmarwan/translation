<?php

namespace SoukTel\Translation\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class TranslationListTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Translation\Models\Translation $translation)
    {
        return [
            'id'                => $translation->getRouteKey(),
            'parent_id'         => $translation->parent_id,
            'start'             => $translation->start,
            'end'               => $translation->end,
            'category'          => $translation->category,
            'translation'              => $translation->translation,
            'time_required'     => $translation->time_required,
            'time_taken'        => $translation->time_taken,
            'priority'          => $translation->priority,
            'status'            => $translation->status,
            'created_by'        => $translation->created_by,
            'created_at'        => $translation->created_at,
        ];
    }
}