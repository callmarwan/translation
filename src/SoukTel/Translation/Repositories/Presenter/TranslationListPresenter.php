<?php

namespace SoukTel\Translation\Repositories\Presenter;

use Litepie\Repository\Presenter\FractalPresenter;

class TranslationListPresenter extends FractalPresenter {

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TranslationListTransformer();
    }
}