<?php

namespace SoukTel\Translation\Repositories\Presenter;

use Litepie\Repository\Presenter\FractalPresenter;

class TranslationItemPresenter extends FractalPresenter {

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TranslationItemTransformer();
    }
}