<?php

namespace SoukTel\Translation\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use SoukTel\Translation\Models\Translation;

class TranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can view the translation.
     *
     * @param User $user
     * @param Translation $translation
     *
     * @return bool
     */
    public function view(User $user, Translation $translation)
    {
        if ($user->canDo('translation.translation.view') && $user->is('admin')) {
            return true;
        }

        return $user->id === $translation->user_id;
    }

    /**
     * Determine if the given user can create a translation.
     *
     * @param User $user
     * @param Translation $translation
     *
     * @return bool
     */
    public function create(User $user)
    {
        return  $user->canDo('translation.translation.create');
    }

    /**
     * Determine if the given user can update the given translation.
     *
     * @param User $user
     * @param Translation $translation
     *
     * @return bool
     */
    public function update(User $user, Translation $translation)
    {
        if ($user->canDo('translation.translation.update') && $user->is('admin')) {
            return true;
        }

        return $user->id === $translation->user_id;
    }

    /**
     * Determine if the given user can delete the given translation.
     *
     * @param User $user
     * @param Translation $translation
     *
     * @return bool
     */
    public function destroy(User $user, Translation $translation)
    {
        if ($user->canDo('translation.translation.delete') && $user->is('admin')) {
            return true;
        }

        return $user->id === $translation->user_id;
    }

    /**
     * @param $user
     * @param $ability
     * @return bool
     */
    public function before($user, $ability)
    {
        if ($user->isSuperUser()) {
            return true;
        }
    }
}
